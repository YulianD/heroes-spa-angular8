
**Front Assessment Angularjs**
-
Cordial greetings, this simple project was made with:
 
 * Angular 8
 * Bootstrap 4
 
 
 **Run development mode:**
 
 `ng serve -o`
 
 
 **Build project:** 
 
 `ng build`
 
 1. _Minified files are hosted in the **dist** directory_
 2. _Take these files to your web server_
