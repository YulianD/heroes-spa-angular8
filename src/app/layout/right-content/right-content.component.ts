import { Component, OnInit } from '@angular/core';
import { HeroesService } from '../../modules/heroes/common/heroes.service';
import { NotificationsService } from 'src/app/core/services/notifications.service';

@Component({
  selector: 'app-right-content',
  templateUrl: './right-content.component.html',
  styleUrls: ['./right-content.component.css']
})
export class RightContentComponent implements OnInit {

  markedHeroes: Array<any> = [];
  selectedHero: object = null;
  newarray: any[];

  constructor(private heroesService: HeroesService, private notifyService: NotificationsService) {
    notifyService.showMyHeroes.subscribe(value => {
      if (value !== null) {
        this.markedHeroes.push(value);
      }
    });

    notifyService.hideMyHeroes.subscribe(value => {
      this.markedHeroes = this.markedHeroes.filter((hero) => {
        return hero.id !== value;
      });
    });

  }

  ngOnInit() {
  }

  showDetails(position: number) {
    this.selectedHero = this.markedHeroes[position];
  }

}
