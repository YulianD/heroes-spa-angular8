import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class NotificationsService {

  public showMyHeroes = new BehaviorSubject(null);
  public hideMyHeroes = new BehaviorSubject(null);

  constructor() {
    this.showMyHeroes.subscribe(value => {
      console.log('Display my heroes:: ', value);
    });
  }

}
