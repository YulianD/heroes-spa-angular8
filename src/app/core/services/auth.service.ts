import { Injectable } from '@angular/core';
import { Observable, of, throwError, BehaviorSubject } from 'rxjs';
import { User } from './user-interface';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private usersList = [];


  constructor(private router: Router) {

  }

  loginSv(formData: any): Observable<any> {
    const users = this.getUsers();

    const result = this.searchUser(formData, users, 'login');
    if (result) {
      this.setUserLogged(this.getUserData(users, formData));
    }

    return of({ result });
  }

  isLogged() {
    const userLogged = this.getUserLogged();
    return (userLogged !== null);
  }

  public getUserLogged() {
    const logged = localStorage.getItem('logged');
    return JSON.parse(logged);
  }

  register(formData: User) {
    this.usersList.push(formData);
    const storedUsers = this.getUsers();
    if (storedUsers == null) {
      this.setUsers(this.usersList);
      this.setUserLogged([formData]);
      this.router.navigate(['/home/heroes']);
    } else {
      if (this.searchUser(formData, storedUsers, 'register')) {
        alert('El Usuario con el email:: ' + formData.email + ' ya se encuentra registrado');
      } else {
        storedUsers.push(formData);
        this.setUsers(storedUsers);
        this.router.navigate(['/auth/login']);
      }

    }

  }

  getUsers() {
    const users = localStorage.getItem('users');

    if (users != null) {
      return JSON.parse(users);
    }
    return null;
  }

  setUserLogged(user) {
    localStorage.setItem('logged', JSON.stringify(user[0]));
  }

  public setUsers(users) {
    const u = JSON.stringify(users);
    localStorage.setItem('users', u);
  }

  searchUser(user: {}, users: [], type: string) {
    const result = [];

    if (type === 'register') {
      users.forEach(item => {
        // @ts-ignore
        if (JSON.stringify(item).toLowerCase().indexOf(user.email.toLowerCase()) > -1) {
          result.push(item);
        }
      });
    } else if (type === 'login') {

      users.forEach(item => {
        // tslint:disable-next-line:max-line-length
        // @ts-ignore
        // tslint:disable-next-line:max-line-length
        if (JSON.stringify(item).toLowerCase().indexOf(user.email.toLowerCase()) > -1 && JSON.stringify(item).toLowerCase().indexOf(user.password.toLowerCase()) > -1) {
          result.push(item);
        }
      });
    }

    return (result.length > 0);
  }

  getUserData(users, user) {
    const result = [];
    users.forEach(item => {
      // tslint:disable-next-line:max-line-length
      // @ts-ignore
      // tslint:disable-next-line:max-line-length
      if (JSON.stringify(item).toLowerCase().indexOf(user.email.toLowerCase()) > -1 && JSON.stringify(item).toLowerCase().indexOf(user.password.toLowerCase()) > -1) {
        result.push(item);
      }
    });

    return result;
  }

  logOut() {
    localStorage.removeItem('logged');
    this.router.navigate(['auth/login']);
  }
}


