import { EventEmitter, Injectable } from '@angular/core';
import { BehaviorSubject, from, Observable, Subject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { AuthService } from '../../../core/services/auth.service';

@Injectable({
  providedIn: 'root'
})
export class HeroesService {

  private API_KEY = '039999ee1634a5e6e9c7f9604a687381';
  private HASH = '24a487c3756bfac92385a2dd4a56249f';
  private URL_API = `https:gateway.marvel.com/v1/public/`;
  private CREDENTIALS = `&ts=1&apikey=${this.API_KEY}&hash=${this.HASH}`;
  private CHARACTERS = 'characters?';

  constructor(private http: HttpClient, private authService: AuthService) {
  }

  getAll(): Observable<any> {
    return this.http.get<any>(`${this.URL_API}${this.CHARACTERS}${this.CREDENTIALS}`)
      .pipe(map((data: any) => data.data.results));
  }

  searchHero(searchTerm: string): Observable<any> {
    const params = `nameStartsWith=${searchTerm}`;
    return this.http.get<any>(`${this.URL_API}${this.CHARACTERS}${params}${this.CREDENTIALS}`).pipe(map((data: any) => data.data.results));
  }

  getMyHeroes(): Array<any> {

    let myHeroes = [];
    const currentUser = this.authService.getUserLogged();
    if ('heroes' in currentUser) {
      myHeroes = currentUser.heroes;
    }
    return myHeroes;
  }

  saveHero(heroe: any) {

    const currentUser = this.authService.getUserLogged();

    const myheroes = this.getMyHeroes();
    if (myheroes.length > 0) {
      currentUser.heroes.push(heroe);
    } else {
      currentUser.heroes = [
        heroe
      ];
    }
    this.authService.setUserLogged([currentUser]);
    // store all users
    const allUsers = this.authService.getUsers();
    const newUsers = allUsers.map((data) => {
      if (data.email === currentUser.email) {
        data.heroes = currentUser.heroes;
      }
      return data;
    });

    this.authService.setUsers(newUsers);
    return this.getMyHeroes();
  }
}
