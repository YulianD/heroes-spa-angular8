import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeroesMarkedComponent } from './heroes-marked.component';

describe('HeroesMarkedComponent', () => {
  let component: HeroesMarkedComponent;
  let fixture: ComponentFixture<HeroesMarkedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HeroesMarkedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeroesMarkedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
