import { Component, Input, OnInit } from '@angular/core';
import { HeroesService } from '../common/heroes.service';
import { FormControl } from '@angular/forms';
import { NotificationsService } from 'src/app/core/services/notifications.service';
import { faThumbtack, faUnlink } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-heroes-marked',
  templateUrl: './heroes-marked.component.html',
  styleUrls: ['./heroes-marked.component.css']
})
export class HeroesMarkedComponent implements OnInit {

  @Input() savedHeroes;
  searchControl = new FormControl();
  icons = {
    faThumbtack, faUnlink
  };

  constructor(private heroesService: HeroesService, private notifyService: NotificationsService) {
  }

  ngOnInit() {

  }

  markHero(position: number) {

    this.notifyService.showMyHeroes.next(this.savedHeroes[position]);
  }
  unmarkHero(id: number) {
    this.notifyService.hideMyHeroes.next(id);
  }

}
