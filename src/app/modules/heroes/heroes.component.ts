import { Component, OnInit } from '@angular/core';
import { HeroesService } from './common/heroes.service';
import { FormControl } from '@angular/forms';
import { NotificationsService } from 'src/app/core/services/notifications.service';
import { faMask, faSave, faBookmark } from '@fortawesome/free-solid-svg-icons';


@Component({
  selector: 'app-heroes',
  templateUrl: './heroes.component.html',
  styleUrls: ['./heroes.component.css']
})
export class HeroesComponent implements OnInit {

  heroesList: Array<any>;
  searchControl = new FormControl();
  markedHeroes: Array<[]>;

  icons = {
    faSave,
    faMask,
    faBookmark
  };

  constructor(
    private heroService: HeroesService,
    private notifyService: NotificationsService,
  ) { }

  ngOnInit() {

    this.markedHeroes = this.heroService.getMyHeroes();

    if (localStorage.getItem('heroes') == undefined) {

      this.heroService.getAll().subscribe((data) => {
        this.heroesList = data;
        localStorage.setItem('heroes', JSON.stringify(data));
      });

    } else {
      this.heroesList = JSON.parse(localStorage.getItem('heroes'));
    }

    this.searchControl.valueChanges.subscribe((term) => {

      if (term.length > 3) {
        this.heroService.searchHero(term).subscribe((result) => {
          this.heroesList = result;
        });
      } else if (term.length === 0) {
        this.heroService.getAll().subscribe((data) => {
          this.heroesList = data;
        });
      }
    });

  }

  saveHero(position: any) {
    this.markedHeroes = this.heroService.saveHero(this.heroesList[position]);
  }
}
