import { Component, OnInit } from '@angular/core';
import { NgForm, FormGroup, FormBuilder, Validators, AbstractControl } from '@angular/forms';
import { AuthService } from '../../../core/services/auth.service';
import { AbstractClassPart } from '@angular/compiler/src/output/output_ast';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  myForm: FormGroup;

  constructor(private authService: AuthService, private myFormBuilder: FormBuilder) {
  }

  ngOnInit() {
    this.fnBuilderForm();
  }

  fnBuilderForm() {
    this.myForm = this.myFormBuilder.group({
      first_name: ['', Validators.required],
      email: ['', [
        Validators.required,
        Validators.email,
        this.fnValidaEmail
      ]],
      password: ['', Validators.required]
    });
  }

  fnRegister() {
    console.log(this.myForm.value);
    this.authService.register(this.myForm.value);
  }

  private fnValidaEmail(control: AbstractControl) {
    const ctrlValue = control.value;
    let error = null;
    if (!ctrlValue.includes('.')) {
      error = { ...error, dot: 'Invalid E-mail' };
    }
    return error;
  }

  getFormErrors(controlName: string): object {
    let error = null;
    const control = this.myForm.get(controlName);
    if (control.touched && control.errors != null) {
      error = control.errors;
    }

    return error;
  }
}
