import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { delay, finalize, tap, catchError } from 'rxjs/operators';
import { Router } from '@angular/router';
import { of } from 'rxjs';
import { AuthService } from '../../../core/services/auth.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;


  constructor(private myService: AuthService, private router: Router, private myFormBuilder: FormBuilder) {
    this.fnBuildform();
  }

  ngOnInit() {
  }

  loginAction() {
    const formData = this.loginForm.value;
    // if (formData.email !== '' && formData.password !== '') {
    this.myService.loginSv(formData).subscribe((data) => {
      if (data.result) {
        console.log(data);
        this.router.navigate(['/home/heroes']);
      } else {
        alert('Credenciales inválidas');
      }
    });
    // }

  }
  fnBuildform() {
    this.loginForm = this.myFormBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required]]
    });
  }

  getFormErrors(controlName: string): object {
    let error = null;
    const control = this.loginForm.get(controlName);
    if (control.touched && control.errors != null) {
      error = control.errors;
    }
    return error;
  }

}
